package io.muic.cloud.myDropbox.user;

public interface UserUtils {

    /**
     * Create User Service method, Add user to DB, Store Hash password
     */
    boolean createUser(String username, String password);

    /**
     * Authenticate User Service method, Authenticate user with DB and Check hashpassword against input password
     */
    boolean authenticate(String username, String password);
}
