package io.muic.cloud.myDropbox.db.hibernate.query;

import io.muic.cloud.myDropbox.db.hibernate.HibernateUtils;
import io.muic.cloud.myDropbox.db.hibernate.entity.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class HibernateQuery {

    /**
     * create user query, with transaction support
     */
    public static Boolean createUser(String username, String hashPassword){
        Session session = HibernateUtils.getSession();;
        try {
            session.beginTransaction();

            User createUser = new User();
            createUser.setUsername(username);
            createUser.setPassword(hashPassword);
            session.persist(createUser);

            session.getTransaction().commit();
        } catch (HibernateException e){
            System.out.println("Error: " + e.getMessage());
            session.getTransaction().rollback();
            return false;
        }
        return true;
    }


    /**
     * find a user by based on their username
     *
     * @throws NoResultException, if user is not found
     */
    public static User findOneUserByUsername(String username) throws NoResultException {
        Session session = HibernateUtils.getSession();

        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<User> query = builder.createQuery(User.class);
        Root<User> table = query.from(User.class);
        query.select(table).where(builder.equal(table.get("username"), username));

//        throw NoResultException if no result is return
        User u = session.createQuery(query).getSingleResult();

        return u;
    }
}
