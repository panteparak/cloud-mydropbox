package io.muic.cloud.myDropbox.aws.dynamodb.util;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import io.muic.cloud.myDropbox.aws.dynamodb.entity.ObjectPermission;

import java.util.ArrayList;
import java.util.List;

public class DynamoDBUtilsImpl implements DynamoDBUtils {

    /**
     * Create a sharing record in DB
     */
    @Override
    public void shareObjectWithUser(String fileRecipient, String fileOwner, String fileLocation) {
        DynamoDBMapper mapper = new DynamoDBMapper(createConnection());
        ObjectPermission objectPermission = new ObjectPermission();
        objectPermission.setUsername(fileRecipient);
        objectPermission.setFileLocation(fileOwner + "/" +fileLocation);
        objectPermission.setOwnership(fileOwner);
        mapper.save(objectPermission);
    }

    /**
     * Check against DB, if file is shared with the user
     */
    @Override
    public Boolean isObjectShareWithUser(String username, String objectOwner,String objectName) {
        DynamoDBMapper mapper = new DynamoDBMapper(createConnection());

        ObjectPermission objectPermission = new ObjectPermission();
        objectPermission.setUsername(username);
        String rangeKey = objectOwner + "/" + objectName;

        Condition condition = new Condition()
                .withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue(rangeKey));

        DynamoDBQueryExpression<ObjectPermission> query = new DynamoDBQueryExpression<ObjectPermission>()
                .withHashKeyValues(objectPermission)
                .withRangeKeyCondition("file_location", condition);

        List<ObjectPermission> out = mapper.query(ObjectPermission.class, query);

        if (out.size() == 1) return out.get(0).getUsername().equals(username) == out.get(0).getFileLocation().equals(rangeKey);
        else return false;
    }

    /**
     * List objects shared with specific user
     *
     * @param username
     */
    @Override
    public List<ObjectPermission> listObjectShareWithUser(String username) {
        DynamoDBMapper mapper = new DynamoDBMapper(createConnection());

        ObjectPermission objectPermission = new ObjectPermission();
        objectPermission.setUsername(username);

        DynamoDBQueryExpression<ObjectPermission> query = new DynamoDBQueryExpression<ObjectPermission>()
                .withHashKeyValues(objectPermission);

//        Return Pagination
        List<ObjectPermission> out = mapper.query(ObjectPermission.class, query);
        List<ObjectPermission> temp = new ArrayList<>();

        for (ObjectPermission t : out){
            temp.add(t);
        }
        return temp;
    }
}
