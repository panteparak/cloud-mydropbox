package io.muic.cloud.myDropbox.fileshare;

import io.muic.cloud.myDropbox.aws.s3.S3Utils;

public interface FileShare {

    /**
     * add file sharing record to DynamoDB
     */
    void addSharePermission(S3Utils s3Utils, String owner, String objectKey, String recipient);
}
