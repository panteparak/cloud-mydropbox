package io.muic.cloud.myDropbox.user;

import io.muic.cloud.myDropbox.db.mysql.MySQLManager;

import java.util.Map;

@Deprecated
public class UserUtilsImpl implements UserUtils, PasswordEncoder {

    private MySQLManager mysql;

    public UserUtilsImpl(MySQLManager mysql) {
        this.mysql = mysql;
    }

    /**
     * Create User Service method, Add user to DB, Store Hash password
     */
    @Override
    public boolean createUser(String username, String password) {
        if (mysql.isUserExist(username)) {
            System.out.println("User: " + username + " is already exist");
            return false;
        }
        return mysql.createUser(username, encodePassword(password));
    }


    /**
     * Authenticate User Service method, Authenticate user with DB and Check hashpassword against input password
     */
    @Override
    public boolean authenticate(String inputUsername, String inputPassword) {
        if (!mysql.isUserExist(inputUsername)) return false;

        Map<String, String> data = mysql.getOneUser(inputUsername);
        if (!data.containsKey("password")) return false;

        String hashPassword = data.get("password");
        return comparePassword(hashPassword, inputPassword);
    }
}
