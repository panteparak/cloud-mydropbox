package io.muic.cloud.myDropbox.aws.dynamodb.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "myDropboxUsers")
public class ObjectPermission {

    @DynamoDBHashKey(attributeName = "username")
    private String username;

    @DynamoDBRangeKey(attributeName = "file_location")
    private String fileLocation;

    @DynamoDBAttribute(attributeName = "file_ownership")
    private String ownership;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    @Override
    public String toString() {
        return "ObjectPermission{" +
                "username=" + this.username +
                " fileLocation=" + this.fileLocation +
                " ownership=" + this.ownership +
                '}';
    }
}
