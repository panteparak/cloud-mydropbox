package io.muic.cloud.myDropbox.aws.s3;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;

public interface S3Utils {

    /**
     * Default implementation of AmazonS3 Connection
     * Connect to Singapore Server with Credential Profile
     */
    default AmazonS3 createConnection() throws SdkClientException, AmazonS3Exception {
        return AmazonS3ClientBuilder.standard()
                .withCredentials(new ProfileCredentialsProvider())
                .withRegion(Regions.AP_SOUTHEAST_1)
                .build();
    }

    /**
     * Add Object to single S3 Bucket, and saved according to username
     */
    void addObjectOfUser(String username, String filePath);

    /**
     * List object of the logged in user
     */
    void listObjectsOfUser(String username);

    /**
     * Download specified object. Object must belong to the user
     */
    void getObjectOfUser(String username, String objectKey);

    /**
     * Download specified object. That is shared with the user
     */
    void getShareObject(String username, String owner,String objectKey);

    /**
     * Check whether a file object exists
     */
    Boolean isObjectExist(String username, String objectKey);
}
