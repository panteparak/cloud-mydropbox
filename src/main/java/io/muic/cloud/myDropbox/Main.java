package io.muic.cloud.myDropbox;

import io.muic.cloud.myDropbox.aws.s3.S3Utils;
import io.muic.cloud.myDropbox.aws.s3.S3UtilsImpl;
import io.muic.cloud.myDropbox.db.hibernate.HibernateUtils;
import io.muic.cloud.myDropbox.fileshare.FileShare;
import io.muic.cloud.myDropbox.fileshare.FileShareImpl;
import io.muic.cloud.myDropbox.user.UserUtils;
import io.muic.cloud.myDropbox.user.UserUtilsHibernateImpl;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {
    private static final String BUCKET_NAME = "mydropbox5780665";

    private static String saveFolder;
    private static UserUtils userUtils;

    /**
     * GlobalState is used as a state tracker
     */
    static class GlobalState{
        private static boolean loggedIn;
        private static String whoami;

        public static boolean isLoggedIn() {
            return loggedIn;
        }

        public static void setLoggedIn(boolean isLoggedIn) {
            GlobalState.loggedIn = isLoggedIn;
        }

        public static String getUsername() {
            return whoami;
        }

        public static void setUsername(String whoami) {
            GlobalState.whoami = whoami;
        }
    }


    public static void main(String[] args) {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String url = Main.class.getProtectionDomain().getCodeSource().getLocation().getFile();
            String decode = URLDecoder.decode(url, "UTF-8");
            saveFolder = new File(decode).getParentFile().getPath();
        } catch (UnsupportedEncodingException | ClassNotFoundException e){
            System.out.println(e.getMessage());
        }

        System.out.println("Welcome to myDropbox");
        System.out.println("type 'help' for list of commands");
        Scanner scanner = new Scanner(System.in);
//        mySQLManager = new MySQLManager(MySQL_URL, MySQL_username, MySQL_password);

//        userUtils = new UserUtilsImpl(mySQLManager);
        userUtils = new UserUtilsHibernateImpl();

        while (true){

            String[] command = cmdParser(scanner.nextLine());

            if (command.length > 0){

                if (command[0].equalsIgnoreCase("newuser") && command.length == 4){
                    String username = command[1];
                    String password = command[2];
                    String repeatPassword = command[3];

                    createUser(username, password, repeatPassword);
                }

                else if (command[0].equalsIgnoreCase("login") && command.length == 3){
                    if (!GlobalState.isLoggedIn()){
                        String username = command[1];
                        String password = command[2];
                        login(username, password);
                    }else {
                        System.out.println("You are currently logged in as " + GlobalState.getUsername());
                    }

                }

                else if (command[0].equalsIgnoreCase("logout")){
                    if (!GlobalState.isLoggedIn()){
                        System.out.println("You are not logged in");
                    }else {
                        logout();
                        System.out.println("Logged out");
                    }
                }

                else if (command[0].equalsIgnoreCase("put") && command.length == 2){
                    if (GlobalState.isLoggedIn()){

                        String file = command[1];
                        putFile(GlobalState.getUsername(), file);
                    } else {
                        System.out.println("You are not log in");
                    }
                }

                else if (command[0].equalsIgnoreCase("view")){
                    if (GlobalState.isLoggedIn()){
                        listFiles(GlobalState.getUsername());
                    } else {
                        System.out.println("You are not log in");
                    }
                }

                else if (command[0].equalsIgnoreCase("get") && command.length == 2){
                    String key = command[1];

                    if (GlobalState.isLoggedIn()){
                        downloadFile(GlobalState.whoami, key);
                    } else {
                        System.out.println("You are not log in");
                    }
                }

                else if (command[0].equalsIgnoreCase("get") && command.length == 3){
                    String key = command[1];
                    String owner = command[2];

                    if (GlobalState.isLoggedIn()){
                        downloadShareFile(GlobalState.whoami, owner, key);
                    }else {
                        System.out.println("You are not log in");
                    }
                }

                else if (command[0].equalsIgnoreCase("quit")){
                    quitProgram();
                }

                else if (command[0].equalsIgnoreCase("help")){
                    helpCommand();
                }

                else if (command[0].equalsIgnoreCase("share") && command.length == 3){
                    if (GlobalState.isLoggedIn()){
                        String recipient = command[2];
                        String objectName = command[1];
                        shareFile(GlobalState.getUsername(), objectName, recipient);
                    }
                }

                else {
                    System.out.println("Invalid Command");
                }
            }
        }
    }

    /**
     * Convert command string into Array of Strings
     */
    private static String[] cmdParser(String strInput){
        StringTokenizer stringTokenizer = new StringTokenizer(strInput);
        List<String> temp = new ArrayList<>();
        while (stringTokenizer.hasMoreTokens()){
            temp.add(stringTokenizer.nextToken());
        }

        return temp.toArray(new String[temp.size()]);
    }

    /**
     * Print Help method
     */
    private static void helpCommand(){
        System.out.println("Create User: newuser {username} {password} {repeat_password}");
        System.out.println("Login: login {username} {password}");
        System.out.println("Logout: logout");
        System.out.println("Upload File: put {absolute_path | relative_path_from_jar}");
        System.out.println("View Files: view");
        System.out.println("Download Files: get {object_key} [username]");
        System.out.println("Share File: share {object_key} {username}");
        System.out.println("Exit Program: quit");
    }

    /**
     * Create User Entry Point method See UserUtils.createUser
     */
    private static void createUser(String username, String password, String repeatPassword){
        if (!password.equals(repeatPassword)){
            System.out.println("Enterd password did not match");
        }else{
            if (userUtils.createUser(username, password))
                System.out.println("User: " + username + " created");
        }
    }

    /**
     * Login Entry Point Method, See UserUtil.authenticate
     */
    private static void login(String username, String password){
        if (userUtils.authenticate(username, password)){
            GlobalState.setLoggedIn(true);
            GlobalState.setUsername(username);

            System.out.println("You are logged in as: " + username);
        }else {
            System.out.println("Invalid Credentials");
        }
    }

    /**
     * Logout Method, Clear GlobalState
     */
    private static void logout(){
        if (GlobalState.isLoggedIn()){
            GlobalState.setUsername("");
            GlobalState.setLoggedIn(false);
        }
    }

    /**
     * putFile entry point method, See SeUtils.addObjectOfUser
     */
    private static void putFile(String username, String file){
        S3Utils s3 = new S3UtilsImpl(saveFolder, BUCKET_NAME);
        s3.addObjectOfUser(username, file);
    }

    /**
     * listFiles entry point method, See SeUtils.listObjectsOfUser
     */
    private static void listFiles(String username){
        S3Utils s3 = new S3UtilsImpl(saveFolder, BUCKET_NAME);
        s3.listObjectsOfUser(username);
    }

    /**
     * downloadFile entry point method, See SeUtils.getObjectOfUser
     */
    private static void downloadFile(String username, String objectName){
        S3Utils s3 = new S3UtilsImpl(saveFolder, BUCKET_NAME);
        s3.getObjectOfUser(username, objectName);
    }

    /**
     * downloadShareFile entry point method
     */
    private static void downloadShareFile(String username, String owner, String objectName){
        if (username.equals(owner)) downloadFile(GlobalState.getUsername(), objectName);
        else {
            S3Utils s3 = new S3UtilsImpl(saveFolder, BUCKET_NAME);
            s3.getShareObject(username, owner,objectName);
        }
    }

    /**
     * shareFile entry point method
     */
    private static void shareFile(String objectOwner, String objectName, String recipient){
        FileShare share = new FileShareImpl();
        share.addSharePermission(new S3UtilsImpl(saveFolder, BUCKET_NAME), objectOwner, objectName, recipient);
    }

    /**
     * Quit Program
     */
    private static void quitProgram(){
        logout();
        HibernateUtils.destroySession();
        System.exit(1);
    }
}
