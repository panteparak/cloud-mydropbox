package io.muic.cloud.myDropbox.user;

import io.muic.cloud.myDropbox.db.hibernate.entity.User;
import io.muic.cloud.myDropbox.db.hibernate.query.HibernateQuery;

import javax.persistence.NoResultException;

public class UserUtilsHibernateImpl implements UserUtils, PasswordEncoder {

    /**
     * Create User Service method, Add user to DB, Store Hash password
     *
     * @param username
     * @param password
     */
    @Override
    public boolean createUser(String username, String password) {
        return HibernateQuery.createUser(username, encodePassword(password));
    }

    /**
     * Authenticate User Service method, Authenticate user with DB and Check hashpassword against input password
     *
     * @param username
     * @param password
     */
    @Override
    public boolean authenticate(String username, String password) {

        User userDAO;
        try {
            userDAO = HibernateQuery.findOneUserByUsername(username);
        } catch (NoResultException e){
            return false;
        }
        return comparePassword(userDAO.getPassword(), password);
    }
}
