package io.muic.cloud.myDropbox.db.hibernate;

import io.muic.cloud.myDropbox.db.hibernate.entity.User;
import org.hibernate.Session;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateUtils {
    private static Session session;
    private static StandardServiceRegistry standardRegistry;

    public static Session getSession(){
        if (session == null || standardRegistry == null || !session.isOpen()){
            standardRegistry = new StandardServiceRegistryBuilder()
                    .build();

            session = new MetadataSources(standardRegistry)
                    .addAnnotatedClass(User.class)
                    .addPackage("io.muic.cloud.myDropbox")
                    .buildMetadata()
                    .buildSessionFactory()
                    .openSession();
        }
        return session;
    }

    public static void destroySession(){
        if (session != null || standardRegistry != null){
            session.close();
            session = null;
            StandardServiceRegistryBuilder.destroy(standardRegistry);
            standardRegistry = null;
        }
    }

    public static void closeSession(){
        if (session != null) {
            session.close();
            session = null;
            standardRegistry = null;
        }
    }
}
