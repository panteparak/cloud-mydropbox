package io.muic.cloud.myDropbox.db.hibernate.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
/**
 * users table
 * stores login credentials
 */

@Entity
@Table(name = "users", indexes = {@Index(columnList = "username", unique = true)})
public class User extends AbstractEntity {

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + this.getId() +
                ", username=" + this.getUsername() +
                ", password=" + this.getPassword() +
                '}';
    }
}
