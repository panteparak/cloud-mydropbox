package io.muic.cloud.myDropbox.aws.dynamodb.util;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import io.muic.cloud.myDropbox.aws.dynamodb.entity.ObjectPermission;

import java.util.List;

public interface DynamoDBUtils {
    /**
     * Create a default connection to DynamoDB, to server in SGP.
     */
    default AmazonDynamoDB createConnection(){
        return AmazonDynamoDBClientBuilder
                .standard()
                .withCredentials(new ProfileCredentialsProvider())
                .withRegion(Regions.AP_SOUTHEAST_1)
                .build();
    }


    /**
     * Create a sharing record in DB
     */
    void shareObjectWithUser(String fileRecipient, String fileOwner, String fileLocation);

    /**
     * Check against DB, if file is shared with the user
     */
    Boolean isObjectShareWithUser(String username, String objectOwner, String objectName);

    /**
     * List objects shared with specific user
     */
    List<ObjectPermission> listObjectShareWithUser(String username);
}
