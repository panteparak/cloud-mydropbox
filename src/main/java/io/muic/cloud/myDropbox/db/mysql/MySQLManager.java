package io.muic.cloud.myDropbox.db.mysql;


import java.sql.*;
import java.util.HashMap;
import java.util.Map;

@Deprecated
public class MySQLManager {
    private Connection connection;

    public MySQLManager(String jdbcURL, String username, String password) {
        try {
            this.connection = DriverManager.getConnection(jdbcURL, username, password);
        } catch (SQLException e){
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * INSERT user into database
     */
    public Boolean createUser(String username, String password){
        String addUserQuery = "INSERT INTO users (username, password) VALUES (?, ?);";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(addUserQuery);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            preparedStatement.executeUpdate();

        } catch (SQLException e){
            System.out.println("Error: " + e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Check if user exist in Database
     */
    public Boolean isUserExist(String username) {
        String hasUserQuery = "SELECT count(*) FROM users WHERE username = ?;";
        try {
            PreparedStatement preparedStatement = connection.prepareCall(hasUserQuery);
            preparedStatement.setString(1, username);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()){
                return rs.getInt(1) > 0;
            }

        } catch (SQLException e){
            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }


    /**
     * Get One user, If duplicate found, will get 1st one (Should not happen)
     */
    public Map<String, String> getOneUser(String username){
        String query = "SELECT username, password FROM users WHERE username = ?;";
        HashMap<String, String> output = new HashMap<>();
        try {

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, username);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()){
                output.put("username", rs.getString("username"));
                output.put("password", rs.getString("password"));
            }

        } catch (SQLException e){
            System.out.println("Error: " + e.getMessage());
        }

        return output;
    }
}
