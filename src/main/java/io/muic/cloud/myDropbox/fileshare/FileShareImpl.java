package io.muic.cloud.myDropbox.fileshare;

import io.muic.cloud.myDropbox.aws.dynamodb.util.DynamoDBUtils;
import io.muic.cloud.myDropbox.aws.dynamodb.util.DynamoDBUtilsImpl;
import io.muic.cloud.myDropbox.aws.s3.S3Utils;

public class FileShareImpl implements FileShare {

    /**
     * add file sharing record to DynamoDB
     */

    @Override
    public void addSharePermission(S3Utils s3Utils, String objectOwner, String objectKey, String recipient) {
        if (!s3Utils.isObjectExist(objectOwner, objectKey)) System.out.println("File does not exist");
        else if (objectOwner.equals(recipient)) System.out.println("Cannot share object with yourself");
        else {
            DynamoDBUtils dynamoDBUtils = new DynamoDBUtilsImpl();
            dynamoDBUtils.shareObjectWithUser(recipient, objectOwner, objectKey);
            System.out.println("File " + objectKey + " Shared with " + recipient);
        }
    }
}
