package io.muic.cloud.myDropbox.aws.s3;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import io.muic.cloud.myDropbox.aws.dynamodb.util.DynamoDBUtils;
import io.muic.cloud.myDropbox.aws.dynamodb.util.DynamoDBUtilsImpl;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

public class S3UtilsImpl implements S3Utils {
    private String bucketName;
    private String baseLocation;

    public S3UtilsImpl(String baseLocation, String bucketName) {
        this.bucketName = bucketName;
        this.baseLocation = baseLocation;
    }

    /**
     * Add Object to single S3 Bucket, and saved according to username
     */
    @Override
    public void addObjectOfUser(String username, String filePath) {
        File obj = new File(filePath);

        if (!obj.isAbsolute()){
            obj = Paths.get(baseLocation, filePath).toFile();

        }

//        System.out.println(obj.toString());

        try {
            String storeLocation = username + "/" + obj.getName();
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, storeLocation, obj);

            TransferManager transferManager = TransferManagerBuilder.standard()
                    .withS3Client(createConnection())
                    .build();

            Upload upload = transferManager.upload(putObjectRequest);
            upload.waitForUploadResult();
            System.out.println("upload completed");


        } catch (SdkClientException e){
            System.out.println("Error: " + e.getMessage());
        } catch (InterruptedException e){
            System.out.println("Upload failed");
        }
    }

    /**
     * List object of the logged in user
     */
    @Override
    public void listObjectsOfUser(String username) {
        try {

            AmazonS3 conn = createConnection();
            if (!conn.doesBucketExist(bucketName)) System.out.println("Specified bucket not found");
            else {
                ListObjectsV2Result result;
                ListObjectsV2Request request = new ListObjectsV2Request().withBucketName(bucketName).withPrefix(username + "/");

                final String FORMAT = "object key: {0} | size: {1} bytes | last-modified: {2} | storage class: {3}";
                MessageFormat messageFormat = new MessageFormat(FORMAT);

                do {
                    result = conn.listObjectsV2(request);

                    List<S3ObjectSummary> s3ObjectSummaryList = result.getObjectSummaries();
                    if (s3ObjectSummaryList.size() == 0){
                        System.out.println("No Personal Files found");
                    }

                    for (S3ObjectSummary file : s3ObjectSummaryList){
                        String out = messageFormat.format(new String[]{
                                StringUtils.replace(file.getKey(), username+ "/", ""),
                                String.valueOf(file.getSize()),
                                file.getLastModified().toString(),
                                file.getStorageClass()
                        });
                        System.out.println(out);
                    }

                    request.setContinuationToken(result.getNextContinuationToken());
                }while (result.isTruncated());

                listShareObject(conn, username);
            }
        } catch (SdkClientException e){
            System.out.println("Error: " + e.getMessage());
        } catch (Exception e){
            System.out.println("Error: " + e.getMessage());
        }
    }

    private void listShareObject(AmazonS3 connection, String username){
        DynamoDBUtils dynamoDBUtils = new DynamoDBUtilsImpl();
        List<String> share = dynamoDBUtils.listObjectShareWithUser(username)
                .stream()
                .map((objectPermission -> objectPermission.getFileLocation()))
                .collect((Collectors.toList()));


        ListObjectsV2Result result;
        ListObjectsV2Request request = new ListObjectsV2Request().withBucketName(bucketName);

        final String FORMAT = "object key: {0} | size: {1} bytes | last-modified: {2} | storage class: {3} | owner: {4}";
        MessageFormat messageFormat = new MessageFormat(FORMAT);

        do {
            result = connection.listObjectsV2(request);

            List<S3ObjectSummary> s3ObjectSummaryList = result.getObjectSummaries();
            if (s3ObjectSummaryList.size() == 0){
                System.out.println("No share files found");
            }

            for (S3ObjectSummary file : s3ObjectSummaryList){
                if (!share.contains(file.getKey())) continue;

                String[] temp = file.getKey().split("/");
                String owner = temp[0];
                String objName = temp[temp.length - 1];


                String out = messageFormat.format(new String[]{
                        objName,
                        String.valueOf(file.getSize()),
                        file.getLastModified().toString(),
                        file.getStorageClass(),
                        owner
                });
                System.out.println(out);
            }

            request.setContinuationToken(result.getNextContinuationToken());
        }while (result.isTruncated());
    }

    /**
     * Download specified object. Object must belong to the user
     */
    @Override
    public void getObjectOfUser(String username, String objectKey) {
        try {
            GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, username + "/" + objectKey);
            S3Object s3Object = createConnection().getObject(getObjectRequest);

            System.out.println("Downloading...");
            FileUtils.copyInputStreamToFile(s3Object.getObjectContent(), Paths.get(baseLocation, StringUtils.replace(s3Object.getKey(), username + "/", "")).toFile());
            System.out.println("Saved file to: " + baseLocation);
        } catch (SdkClientException | IOException e){
            System.out.println("Error: " + e.getMessage());
        } catch (Exception e){
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
     * Check whether a file object exists
     *
     * @param username
     * @param objectKey
     */
    @Override
    public Boolean isObjectExist(String username, String objectKey) {
        return createConnection().doesObjectExist(bucketName, username + "/" + objectKey);
    }

    /**
     * Download specified object. That is shared with the user
     *
     * @param username
     * @param objectKey
     * @param owner
     */
    @Override
    public void getShareObject(String username, String owner,String objectKey) {
        DynamoDBUtils dbUtils = new DynamoDBUtilsImpl();
        if (dbUtils.isObjectShareWithUser(username, owner, objectKey)){
            getObjectOfUser(owner, objectKey);
        }else System.out.println("Object is not shared with you");
    }

    public static void main(String[] args) {
        S3UtilsImpl s3Utils = new S3UtilsImpl("", "mydropbox5780665");
        s3Utils.listShareObject(s3Utils.createConnection(), "pteparak");
    }
}
