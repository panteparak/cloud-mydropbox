package io.muic.cloud.myDropbox.user;

import org.mindrot.jbcrypt.BCrypt;

public interface PasswordEncoder {

    /**
     * Hash password using BCrypt, 10 Hash rounds
     */
    default String encodePassword(String rawPassword){
        return BCrypt.hashpw(rawPassword, BCrypt.gensalt());
    }


    /**
     * Compare hashed password with input password
     */
    default boolean comparePassword(String hashPassword, String rawPassword) {
        return BCrypt.checkpw(rawPassword, hashPassword);
    }
}
